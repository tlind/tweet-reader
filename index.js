var { readFile } = require('./services/data-service');
var { deserializeUsers } = require('./services/user-deserialization');
var { deserializeTweets } = require('./services/tweet-deserialization');
var { generateFeed } = require('./services/feed-generator');
var userIterables = require('./model/user-iterables');
var consoleSerializer = require('./services/console-serialization');

async function main() {
  try {
    // load from files
    var userStr = await readFile('var/user.txt');
    var tweetStr = await readFile('var/tweet.txt');

    // deserialise string to struct
    var users = await deserializeUsers(userStr);
    var tweets = await deserializeTweets(tweetStr);

    // ensure users are iterated by asc username
    for (let user of userIterables.byAscUsername(users)) {
      // generate feed
      var feed = generateFeed({ forUser: user, tweets });

      // serialize to console
      consoleSerializer.logFeedForUser(console.log.bind(console), feed);
    }
  } catch(e) {
    console.error(e);
    process.exit(e.errno);
  }
}

if (require.main === module) {
  main();
}
