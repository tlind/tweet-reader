var User = require('../model/user');

/* Don't assume that a user in the followed list has a line defining who they follow.
    All usernames are case sensitive while loading, although may be compared insensitively once loaded. */
exports.deserializeUsers = function(userStr) {
  var lines = userStr.split("\n");
  var users = {};

  for (let line of lines) {
    // skip blank lines
    if (!line.trim()) continue;

    let username = line.split(' ')[0];

    let followsStr = line.substr(username.length + (' follows '.length));
    let follows = followsStr.split(',');
    follows = follows.map((name)=>name.trim());

    // iterate over the user's this user is following
    for (let followingUsername of follows) {
      // skip blanks
      if (!followingUsername) continue;

      // don't assume that the followed user has their own line
      if (!(followingUsername in users)) {
        users[followingUsername] = new User({ username: followingUsername })
      }
    }

    // create user object if necessary
    if (!(username in users)) {
      users[username] = new User({ username });
    }
    // merge following
    users[username].addFollowers(follows);
  }

  // convert to array
  return Object.keys(users).map((username) => users[username]);
};
