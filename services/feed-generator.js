
/*
  The tweets are assumed to be ordered by asc timestamps,
    the feed returned is also ordered by asc timestamps (unlike twitter).
*/
exports.generateFeed = function({ forUser, tweets }) {
  var feed = {
    forUser,
    tweets: []
  };

  for (let tweet of tweets) {
    // note: hasFollower is used so username comparison is case insensitive,
    // and also passes if this tweet is by the user themself
    if (forUser.isFollowing(tweet.username)) {
      feed.tweets.push(tweet);
    }
  }
  return feed;
}
