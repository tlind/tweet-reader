
exports.logFeedForUser = function(log, feed) {
  log(feed.forUser.username);
  for (let tweet of feed.tweets) {
    log(`\t@${tweet.username}: ${tweet.message}`);
  }
}
