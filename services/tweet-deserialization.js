var Tweet = require('../model/tweet');

/*
  Tweets are considered to be posted in the order they appear in this file,
    i.e the file is ordered asc timestamps.
    The array of tweets returned is ordered by asc timestamps.
*/
exports.deserializeTweets = function(tweetStr) {
  var lines = tweetStr.split("\n")
  var tweets = [];

  for (let line of lines) {
    if (!line) continue;

    var indexOfSeparator = line.indexOf('> ');
    if (indexOfSeparator === -1) {
      console.warn("Skipping invalid tweet");
      continue;
    }

    var username = line.substr(0, indexOfSeparator);
    if (!username) {
      console.warn("Skipping invalid tweet");
      continue;
    }

    var message = line.substr(indexOfSeparator + 2);
    tweets.push(new Tweet({ username, message, date: new Date() }));
  }

  return tweets
}
