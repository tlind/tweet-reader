var fs = require('fs');

exports.readFile = async function(filename) {
    return new Promise((resolve, reject) => {
      fs.readFile(filename, 'utf8', (e, data) => {
        if (e) return reject(e);

        resolve(data);
      });
    });
}
