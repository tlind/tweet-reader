var assert = require('assert');
var dataService = require('../services/data-service');
var userDeserializer = require('../services/user-deserialization');
var tweetDeserializer = require('../services/tweet-deserialization');
var { generateFeed } = require('../services/feed-generator');
var consoleSerializer = require('../services/console-serialization');
var User = require('../model/user');
var Tweet = require('../model/tweet');
var userIterables = require('../model/user-iterables');

describe('#readFile', function() {

  it('should read a file into string', async function() {
    var userStr = await dataService.readFile('var/user.txt');
    assert (typeof userStr === 'string');
  });

});

describe('#deserializateUsers', function() {

  describe('valid data', function() {

    var userStr, users;

    before(async function() {
      userStr = await dataService.readFile('var/user.txt');
      users = userDeserializer.deserializeUsers(userStr);
    });

    it('should load valid user data into data struct', function() {
      assert(users instanceof Array);
      var alan = users.find((user) => user.username === 'Alan');
      var ward = users.find((user) => user.username === 'Ward');
      var martin = users.find((user) => user.username === 'Martin');
      assert(typeof alan !== 'undefined');
      assert(typeof ward !== 'undefined');
      assert(typeof martin !== 'undefined');
      assert(users.length === 3);
    });
    it('should sort users alphabetically', function() {
      var prev = null;
      for (let user of userIterables.byAscUsername(users)) {
        if (prev) {
          assert(user.username >= prev.username);
        }

        prev = user;
      }
    });
    it('should merge followers without duplicates', function() {
      // ensure ward exists and follows Alan, once
      var ward = users.find((user) => user.username === 'Ward');
      assert(typeof ward !== 'undefined');
      assert(ward.follows.has('Alan'));
    });
  });
});

describe('#deserializeTweets', function() {
  var tweetStr, tweets;

  before(async function() {
    tweetStr = await dataService.readFile('var/tweet.txt');
    tweets = tweetDeserializer.deserializeTweets(tweetStr);
  });

  it('should load valid data stream into data struct', function() {
    assert(tweets instanceof Array);
    assert(tweets.length === 3);

    var alans = tweets.filter((tweet)=> tweet.username === 'Alan');
    assert(alans.length === 2);
    var wards = tweets.filter((tweet)=> tweet.username === 'Ward');
    assert(wards.length === 1);
  });

  it('should sort tweets chronologically asc', function() {
    var prev = null;
    for (let tweet of tweets) {
      if (prev) {
        assert(tweet.date <= prev.date);
      }

      prev = tweet;
    }
  });
});

describe('#generateFeed', function() {

  var user = new User({ username: "tester" });
  user.addFollowers(["followedByTester"]);

  var tweets = [
    new Tweet({ username: "followedByTester", message: "hi" }),
    new Tweet({ username: "notFollowedByUser", message: "are you talking to me?" }),
    new Tweet({ username: "tester", message: "hi yourself" })
  ];

  var feed = generateFeed({ forUser: user, tweets });

  it('should include all tweets from the accounts this user follows and own tweets', function() {
    assert(feed.tweets[0].username === "followedByTester");
    assert(feed.tweets[1].username === "tester");
  });
  it('should not include tweets from accounts that are not followed', function() {
    assert(feed.tweets.length === 2);
  });
  it('should display tweets in descending chronological order', function() {
    assert(feed.tweets[0].username === "followedByTester");
    assert(feed.tweets[1].username === "tester");
  });

  describe('consoleSerializer', function() {
      describe('#logFeedForUser', function() {
        var logged = '';
        function log(str) {
          logged += str + "\n";
        }

        consoleSerializer.logFeedForUser(log, feed);

        it ("should output a feed", function() {
          assert(logged === "tester\n\t@followedByTester: hi\n\t@tester: hi yourself\n");
        });
      });
  });

});
