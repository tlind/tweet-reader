
// Using iterators allows for different ordering,
// improves readability and ensures callers expectations
exports.byAscUsername = function(users) {
  return {
    *[Symbol.iterator]() {
      var usersAsc = users.slice().sort((a,b) => a.username > b.username);

      yield* usersAsc;
    }
  };
}
