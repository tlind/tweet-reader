function User({ username }) {
  // defaults
  Object.assign(this, {
    username: '',
    follows: new Set() // usernames
  })
  this.username = username;
};

User.compareUsernames = function compareUsernames(u1, u2) {
  return new RegExp("^"+u2+"$", 'i').test(u1);
};

// add followers without creating duplicates
User.prototype.addFollowers = function(usernames) {
  for (let username of usernames) {
    this.follows.add(username);
  }
};

// case insensitive check for whether a user is following another,
// also checks if this is the user themself, in which case isFollowing === true
User.prototype.isFollowing = function(username) {
    if (User.compareUsernames(username, this.username)) {
      return true;
    }

    for (let follows of this.follows) {
      if (User.compareUsernames(follows, username)) return true;
    }


    return false;
};

module.exports = User;
