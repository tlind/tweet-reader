function Tweet({ username, message, date }) {
  this.username = username;
  this.message = message;
  this.date = date;
}

module.exports = Tweet;
